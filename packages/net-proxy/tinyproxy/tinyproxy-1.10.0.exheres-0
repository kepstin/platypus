# Copyright (c) 2010 Jan Meier
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.xz ]
require systemd-service
require openrc-service

SUMMARY="Tinyproxy is a light-weight HTTP proxy daemon for POSIX operating systems."
HOMEPAGE="https://tinyproxy.github.io"

SLOT="0"
LICENCES="GPL-2"
PLATFORMS="~amd64 ~x86"
DEPENDENCIES="
    build:
        app-doc/asciidoc
    build+run:
        group/tinyproxy
        user/tinyproxy
"
MYOPTIONS="systemd"

# PID-file will be created in /var/run, but /var/run is a link to /run.
# in future, tinyproxy needs an other config, that it uses /run.
# localstatedir can't be changed to / because /var/log.
DEFAULT_SRC_CONFIGURE_PARAMS="--localstatedir=/var"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/user.patch"
    "${FILES}/pid.patch"
)

src_install() {
    default

    keepdir /var/log/tinyproxy
    insinto /etc/logrotate.d
    newins "${FILES}/${PN}.logrotate" "${PN}"

    if option systemd; then
        install_systemd_files
        insinto /usr/$(exhost --target)/lib/tmpfiles.d
        hereins tinyproxy.conf <<EOF
d /run/tinyproxy 0770 tinyproxy tinyproxy
EOF
    fi
    install_openrc_files
}

